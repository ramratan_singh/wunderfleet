$(function(){

    $("#form-total").steps({
        headerTag: "h2",
        bodyTag: "section",
        transitionEffect: "fade",
        enableAllSteps: true,
        autoFocus: true,
        transitionEffectSpeed: 500,
        titleTemplate : '<div class="title">#title#</div>',
        labels: {
            previous : 'Back',
            next : '<i class="zmdi zmdi-chevron-right"></i>',
            finish : '<i class="zmdi zmdi-chevron-right"></i>',
            current : ''
        },
        onStepChanging: function (event, currentIndex, newIndex) { 

            // start last page to redirect first and second steps
            if(currentIndex == '2'){
                return true;
            }
            // end last page to redirect first and second steps

            // start step 1
            var first_name = $("#first_name").val();
            var last_name = $("#last_name").val();
            var phone = $("#phone").val();
            if(first_name == ''){
            $.alert("First Name is required");
            return false;
            }
            else if(last_name == ''){
            $.alert("Last Name is required");
            return false;
            }
            else if(phone == ''){
            $.alert("Telephone is required");
            return false;
            }
            else if(phone.length<10){
                $.alert("Please Enter Valid Telephone");
                return false;
            }
                          
            var unique_id = window.localStorage.getItem('unique_id'); // check ID already save or not
            
            if(currentIndex == '0' && (!first_name || !last_name || !phone)){
              return false;   
            }
            else if(currentIndex == '0' && (first_name && last_name && phone)){
              if(!unique_id){
                  $.ajax({
                    url:"controllers/indexcontroller.php?action=insert_information",
                    method:"POST",
                    data:{first_name:first_name,last_name:last_name,phone:phone},
                    success:function(data)
                    {
                     if(data){
                     if(data.search('denied') == '-1'){ // database connection checked
                     let res = JSON.parse(data);
                     if(res && res.unique_id){
                     window.localStorage.setItem('unique_id',res.unique_id); 
                     window.localStorage.setItem('steps','1');    // save steps in localstorage
                     
                     // save step 1 data in localstoarge
                     let custData = {
                          first_name : first_name,
                          last_name : last_name,
                          phone : phone
                     }
                     window.localStorage.setItem("custData", JSON.stringify(custData));
                     $('a[href="#next"]').click();
                     // save step 1 data in localstoarge
                        }
                      }
                       else{
                        $.alert("oops something went wrong please try again, Check Your Database Login Credentials");
                        return false;
                      }
                     }
                    }
                 }); 
              }
              if(unique_id){
              return true;
              }
            }

            // end step 1    
        
            // start step 2        
            var address = $("#address").val();
            var street = $("#street").val();
            var house_number = $("#house_number").val();
            var zipcode = $("#zipcode").val();
            var city = $("#city").val();
            if(address == '' && unique_id){
                $.alert("Please Enter Address");
                return false;
            }
            else if(street == '' && unique_id){
                $.alert("Please Enter Street");
                return false;
            }
            else if(house_number == '' && unique_id){
                $.alert("Please Enter House Number");
                return false;
            }
            else if(zipcode == '' && unique_id){
                $.alert("Please Enter Zip Code");
                return false;
            }
            else if(city == '' && unique_id){
                $.alert("Please Enter City");
                return false;
            }
            if(currentIndex == '1' && (!address || !street || !house_number || !zipcode || !city )){
              return false;   
            }
            else if(currentIndex == '1' && (address && street && house_number && zipcode && city )){
                if(unique_id){
                  $.ajax({
                    url:"controllers/indexcontroller.php?action=update_address_information",
                    method:"POST",
                    data:{address:address,street:street,house_number:house_number,zipcode:zipcode,city:city,customerId:unique_id},
                    success:function(data)
                    {
                      if(data){
                        let custData = JSON.parse(window.localStorage.getItem("custData")); // exist cust data
                        
                        // add steps 2 paramter
                        custData.address = address;
                        custData.street = street;
                        custData.house_number = house_number;
                        custData.zipcode = zipcode;
                        custData.city = city;
                        // add steps 2 paramter
                        
                        window.localStorage.setItem("custData", JSON.stringify(custData)); // update cust data in steps 2 localstorage
                        window.localStorage.setItem("steps","2");    // save steps in localstorage
                             
                      }
                    }
                 }); 
              }
              return true;
            }
            // end step 2
            
           
        },
        
        onFinished: function (event, currentIndex) {
            // start step 3
            var account_owner = $("#account_owner").val();
            var iban = $("#iban").val();
            if(account_owner == ''){
            $.alert("Account Owner is required");
            return false;
            }
            else if(iban == ''){
            $.alert("IBAN is required");
            return false;
            }
            if(currentIndex == '2' && (!account_owner || !iban )){
              return false;
            }
            else if(currentIndex == '2' && (account_owner && iban )){
            var unique_id = window.localStorage.getItem('unique_id'); // check ID already save or not
                $.ajax({
                    url:"controllers/indexcontroller.php?action=update_payment_information",
                    method:"POST",
                    data:{account_owner:account_owner,iban:iban,customerId:unique_id},
                    success:function(data)
                    {
                      if(data){
                        let res = JSON.parse(data);
                        if(res &&  res.message){
                           $.alert(res.message);
                           return false;  
                        }
                        else{
                        // start clear localStorage
                         window.localStorage.removeItem("custData"); 
                         window.localStorage.removeItem("steps");   
                         window.localStorage.removeItem("unique_id");
                        // end clear localStorage
                        
                        window.location.href="view/success.php?paymentDataId="+res.paymentDataId;
                        }
   
                      }
                    }
                 }); 

            }
            // end step 3
        }
    });
    
    // start already exist customer id checked and set data input field
    var unique_id = window.localStorage.getItem('unique_id');
    if(unique_id){
        let custData = JSON.parse(window.localStorage.getItem("custData"));
        let steps  = window.localStorage.getItem("steps");
        if(steps == 2){
            $("#first_name").val(custData.first_name);
            $("#last_name").val(custData.last_name);
            $("#phone").val(custData.phone);
            $("#address").val(custData.address);
            $("#street").val(custData.street);
            $("#house_number").val(custData.house_number);
            $("#zipcode").val(custData.zipcode);
            $("#city").val(custData.city);
            $('a[href="#next"]').click(); // move step 2
            $('a[href="#next"]').click(); // move step 3

        }
        else{
            $("#first_name").val(custData.first_name);
            $("#last_name").val(custData.last_name);
            $("#phone").val(custData.phone); 
            $('a[href="#next"]').click(); // move step 2
        }
    }
    // end already exist customer id checked and set data input field
});

    // start common function accept numeric values only
    function validate(evt) {
      var theEvent = evt || window.event;
    
      // Handle paste
      if (theEvent.type === 'paste') {
          key = event.clipboardData.getData('text/plain');
      } else {
      // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
      }
      var regex = /[0-9]|\./;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
      }
    }
    // start common function accept numeric values only


