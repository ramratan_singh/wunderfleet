<?php
     
    // start save personal information   
	function save_information($data)
	{   
      include '../config/db.php';
      // start check database connection
      if (!$conn) {
        $res['status'] = false;
        return json_encode($res);
      }
      // end check database connection

      $sql = "INSERT INTO wunderfleet(first_name,last_name,phone) VALUES(:first_name,:last_name,:phone)";
      $statement = $conn->prepare($sql);	
      $statement->bindParam(":first_name", $data['first_name']);
      $statement->bindParam(":last_name", $data['last_name']);
      $statement->bindParam(":phone", $data['phone']);
      if($statement->execute()){
        $res['unique_id'] = $conn->lastInsertId();
        $res['status'] = true;
        return json_encode($res);
      }
	}
    // end save personal information   
    
    
    // start save address information   
	function save_address_information($data)
	{   
      include '../config/db.php';
      $sql = "UPDATE wunderfleet SET address=:address,street=:street,house_number=:house_number,zipcode=:zipcode,city=:city WHERE customerId=:customerId";
      $statement = $conn->prepare($sql);	
      $statement->bindParam(":address", $data['address']);
      $statement->bindParam(":street", $data['street']);
      $statement->bindParam(":house_number", $data['house_number']);
      $statement->bindParam(":zipcode", $data['zipcode']);
      $statement->bindParam(":city", $data['city']);
      $statement->bindParam(":customerId", $data['customerId']);
      if($statement->execute()){
      return true;
      }
	}
    // end save address information  
    
    
    // start save payment information   
	function save_payment_information($data)
	{   
      include '../config/db.php';
      $sql = "UPDATE wunderfleet SET account_owner=:account_owner,iban=:iban WHERE customerId=:customerId";
      $statement = $conn->prepare($sql);	
      $statement->bindParam(":account_owner", $data['account_owner']);
      $statement->bindParam(":iban", $data['iban']);
      $statement->bindParam(":customerId", $data['customerId']);
      if($statement->execute()){
        $paymentDataId = rand(); ; //generate random payment data ID
        
        // start update payment data ID in database
          $update_paymentid_sql = "UPDATE wunderfleet SET paymentDataId=:paymentDataId WHERE customerId=:customerId";
          $statement = $conn->prepare($update_paymentid_sql);	
          $statement->bindParam(":paymentDataId", $paymentDataId);
          $statement->bindParam(":customerId", $data['customerId']);
           if($statement->execute()){
               $res['paymentDataId'] = $paymentDataId;
               return json_encode($res);
           }
        // end update payment data ID in database
        
        // start save-payment-data using curl 
        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL,"https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-b ackend-dev-save-payment-data");
        // # Setup request to send json via POST.
        // $payload = json_encode( array( "customerId"=> $data['customerId'], "iban"=> $data['iban'], "owner"=> $data['account_owner']));
        // curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        // // curl_setopt($ch, CURLOPT_POSTFIELDS,"customerId=".$data['customerId']."&iban=".$data['iban']."&owner=".$data['account_owner']);
        // curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        // // Receive server response ...
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // $server_output = curl_exec($ch);
        // curl_close ($ch);
        // return $server_output;
        // end save-payment-data curl
        
      }
	}
    // end save payment information  
    



?>