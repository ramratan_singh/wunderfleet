<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Wunderfleet</title>
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Font-->
	<link rel="stylesheet" type="text/css" href="assets/css/nunito-font.css">
	<link rel="stylesheet" type="text/css" href="assets/fonts/material-design-iconic-font/css/material-design-iconic-font.min.css">
	<!-- Main Style Css -->
    <link rel="stylesheet" href="assets/css/style.css"/>
    <link rel="stylesheet" href="assets/css/jquery-confirm.min.css?v=1.0.0"/>
</head>

<body>
	<div class="page-content">
		<div class="wizard-v5-content">
			<div class="wizard-form">
		        <form class="form-register" id="form-register" action="<?php echo URL; ?>index/insert_information" method="post" >
		        	<div id="form-total">
		        		<!-- SECTION 1 -->
			            <h2>
			            	<span class="step-icon"><i class="zmdi zmdi-check"></i></span>
			            	<span class="step-text">Personal Information</span>
			            </h2>
			            <section>
			                <div class="inner">
								<div class="form-row">
									<div class="form-holder form-holder-2">
										<label for="first_name">First Name</label>
										<input type="text" class="form-control" id="first_name" name="first_name" required>
									</div>
								</div>
								<div class="form-row">
									<div class="form-holder form-holder-2">
										<label for="last_name">Last Name</label>
										<input type="text"  class="form-control" id="last_name" name="last_name" required>
									</div>
								</div>
								<div class="form-row">
									<div class="form-holder form-holder-2">
										<label for="phone">Telephone</label>
										<input type="text" class="form-control" id="phone" name="phone" minlength="10" maxlength="10"  onkeypress="validate(event)"  required>
									</div>
								</div>
							</div>
			            </section>
			            	<!-- SECTION 2 -->
			            <h2>
			            	<span class="step-icon"><i class="zmdi zmdi-check"></i></span>
			            	<span class="step-text">Address Information</span>
			            </h2>
			            <section>
			                <div class="inner">
			                    <div class="form-row">
									<div class="form-holder form-holder-2">
										<label for="address">Address Location</label>
										<input type="text" class="form-control" id="address" name="address" required>
										<span><i class="zmdi zmdi-pin"></i></span>
									</div>
								</div>
	                            <div class="form-row">
									<div class="form-holder">
										<label for="code">Street</label>
										<input type="text" class="form-control" id="street" name="street" required>
									</div>
									<div class="form-holder">
										<label for="code">House Number</label>
										<input type="text" class="form-control" id="house_number" name="house_number" required>
									</div>
								</div>
								<div class="form-row">
									<div class="form-holder">
										<label for="code">Zip Code</label>
										<input type="text" class="form-control" id="zipcode" name="zipcode" minlength="6" maxlength="8"  onkeypress="validate(event)" required>
									</div>
									<div class="form-holder">
										<label for="code">City</label>
										<input type="text" class="form-control" id="city" name="city" required>
									</div>
								</div>
							</div>
			            </section>
						<!-- SECTION 3 -->
			            <h2>
			            	<span class="step-icon"><i class="zmdi zmdi-check"></i></span>
			            	<span class="step-text">Payment Information</span>
			            </h2>
			            <section>
			                <div class="inner">
								<div class="form-row">
									<div class="form-holder form-holder-2 m-1">
										<label for="account_number">Account Owner</label>
										<input type="text" class="form-control input-step-2-2" id="account_owner" name="account_owner" required>
										<span class="card"><i class="zmdi zmdi-card"></i></span>
									</div>
								</div>
								<div class="form-row">
									<div class="form-holder form-holder-2 m-1">
										<label for="user_id" class="special-label">IBAN</label>
										<input type="text" class="form-control" id="iban" name="iban" required>
									</div>
								</div>
							</div>
			            </section>
		        	</div>
		        </form>
			</div>
		</div>
	</div>
<!--start script-->
<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/jquery.steps.js"></script>
<script src="assets/js/main.js?v=1.0.1"></script>
<script src="assets/js/jquery-confirm.min.js?v=1.0.1"></script>
<!--end script-->
</body>
</html>